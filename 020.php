<?php 
//Задача 20
// n! means n × (n − 1) × ... × 3 × 2 × 1
// For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
// and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
// Find the sum of the digits in the number 100!
echo 'Задача 20<br>';
$factorial = 100;
$res = '1';
$sum = 0;
for ($i = 1; $i <= $factorial; $i++) {
	$res = bcmul($res, $i);
}
$len = strlen($res);
for ($j = 0; $j < $len; $j++) {
	$sum += $res[$j];
}
echo $sum . '<br>'; //648
?>