<?php 
//Задача 28
// Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

// 21 22 23 24 25
// 20  7  8  9 10
// 19  6  1  2 11
// 18  5  4  3 12
// 17 16 15 14 13

// It can be verified that the sum of the numbers on the diagonals is 101.

// What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
echo 'Задача 28<br>';
$start = microtime(true);
$maxSide = 1001;
$sum = 1; // count center number
for ($n = 3; $n <= $maxSide; $n += 2) {
	$rightUp = $n * $n;
	$leftUp = $rightUp - ($n - 1);
	$leftDown = $rightUp - 2 * ($n - 1);
	$rightDown = $rightUp - 3 * ($n - 1);
	$sum += $rightUp + $leftUp + $leftDown + $rightDown;
}
echo 'Ответ = ' . $sum . ' <br>'; // 669171001
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>