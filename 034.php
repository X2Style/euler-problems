<?php 
//Задача 34
// 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

// Find the sum of all numbers which are equal to the sum of the factorial of their digits.

// Note: as 1! = 1 and 2! = 2 are not sums they are not included.
echo 'Задача 34<br>';
$start = microtime(true);
$factorial[0] = 1;
$result = 0;
for ($i = 1; $i <= 9; $i++) {
	$factorial[$i] = $factorial[$i - 1] * $i;
}
// 9! * 9 = 3265920 (7 digits) -> limit = 9! * 7 = 2540160
for ($i = 3; $i <= $factorial[9] * 7; $i++) {
	$sum = 0;
	// $n = $i;
	// while ($n > 0) {
		// $v = $n % 10;
		// $n = floor($n/10);
		// $sum += $factorial[$v];
	// }
	//Twise faster than ^^^^^
	$arrayI = str_split($i . '');
	foreach ($arrayI as $v) {
		$sum += $factorial[$v];
	}
	if ($sum == $i) {
		$result += $sum;
	}
}
echo 'Ответ: ' . $result . '<br>'; // 40730
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>