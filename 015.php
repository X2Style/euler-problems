<?php 
//Задача 15
// Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.
// How many such routes are there through a 20×20 grid?
echo 'Задача 15<br>';
$gridX = 20;
$gridY = 20;

for ($i = 0; $i <= $gridX; $i++) {
	$gridXY[$i][0] = 1;
}
for ($j = 0; $j <= $gridY; $j++) {
	$gridXY[0][$j] = 1;
}
for ($i = 1; $i <= $gridX; $i++) {
	for ($j = 1; $j <= $gridY; $j++) {
		$gridXY[$i][$j] = $gridXY[$i-1][$j] + $gridXY[$i][$j-1];
		/*echo 'X = ' . $i . ' Y = ' . $j . ' Value = ' . $gridXY[$i][$j] . '<br>';*/
	}
}
echo 'Количество путей: ' . $gridXY[20][20]; // 137846528820
?>