<?php 
//Задача 14
// The following iterative sequence is defined for the set of positive integers:

// n → n/2 (n is even)
// n → 3n + 1 (n is odd)

// Using the rule above and starting with 13, we generate the following sequence:

// 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
// It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

// Which starting number, under one million, produces the longest chain?

// NOTE: Once the chain starts the terms are allowed to go above one million.
echo 'Задача 14<br>';
$start = microtime(true);
$number = 1000000;
$maxLen = 0;
$maxI = 0;
for ($i = 1; $i < $number; $i++) {
	$len = 1;
	$num = $i;
	while ($num > 1) {
		if ($num % 2 == 0) {
			$num /= 2;
		} else {
			$num = $num * 3 + 1;
		}
		/*if ($num > $i && $num < $number) {
			$i = $num;
		}*/
		$len++;
		/*echo $num . '<br>';*/
	}
	if ($len > $maxLen) {
		$maxLen = $len;
		$maxI = $i;
	}
}
echo 'Максимальная цепочка ' . $maxLen . '<br>'; //525
echo 'Максимальное число ' . $maxI . '<br>'; //837799
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>'; // 51.33 s
?>