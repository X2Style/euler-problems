<?php 
//Задача 7
// By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
// What is the 10 001st prime number?
echo 'Задача 7<br>';
// My solution
$start = microtime(true);
$number = 10001;
$primes[1] = 2;
$i = 3;
while (!isset($primes[$number])) {
	$simple = true;
	foreach ($primes as $prime) {
		if ($i % $prime == 0) {
			$simple = false;
			break;
		}
	}
	if ($simple) {
		$primes[] = $i;
	}
	$i += 2;
}
echo 'Простое число: ' . $primes[$number] . '<br>'; //104743
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>'; // 7.85 s

// function from Euler
function isPrime($n) {
	if ($n == 1) {
		return false;
	} 
	elseif ($n < 4){
		return true;
	}
	elseif ($n % 2 == 0) {
		return false;
	}
	elseif ($n < 9) {
		return true;
	}
	elseif ($n % 3 == 0) {
		return false;
	}
	else {
		$r = floor(sqrt($n));
		$f = 5;
		 while ($f <= $r) {
			 if ($n % $f == 0) {
				 return false;
			 }
			 if ($n % ($f + 2) == 0) {
				 return false;
			 }
			 $f += 6;
		 }
		return true;
	}
}
$start = microtime(true);
$limit = 10001;
$count = 1;
$candidate = 1;
while ($count < $limit) {
	$candidate += 2;
	if (isPrime($candidate)) {
		$count++;
	}
}
echo 'Простое число: ' . $candidate . '<br>'; //104743
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>'; // 0.59 s
?>