<?php 
//Задача 22
// Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.

// For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.

// What is the total of all the name scores in the file?

echo 'Задача 22<br>';
$sum = 0;
$fileName = 'names.txt';
$file = fopen($fileName, 'r');
$data = str_replace('"', '', fread($file, filesize($fileName)));
$data = explode(',', $data);
sort($data);
foreach ($data as $key => $name) {
	$nameCode = 0;
	for ($i = 0; $i < strlen($name); $i++) {
		$nameCode += ord($name[$i]) - 64;
	}
	$sum += $nameCode * ($key + 1);
}
echo $sum . '<br>'; //871198282

// function quicksort (&$array, $l=0, $r=0) {
    // if($r === 0) $r = count($array)-1;
    // $i = $l;
    // $j = $r;
    // $x = $array[($l + $r) / 2];
    // do {
        // while ($array[$i] < $x) $i++;
        // while ($array[$j] > $x) $j--;
        // if ($i <= $j) {
            // if ($array[$i] > $array[$j])
                // list($array[$i], $array[$j]) = array($array[$j], $array[$i]);
            // $i++;
            // $j--;
        // }
    // } while ($i <= $j);
    // if ($i < $r) quicksort ($array, $i, $r);
    // if ($j > $l) quicksort ($array, $l, $j);
// }
?>