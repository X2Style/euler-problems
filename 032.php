<?php 
//Задача 32
// We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.

// The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.

// Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.

// HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.
echo 'Задача 32<br>';
$start = microtime(true);
$sum = 0; 
$products = [];
for ($i = 1; $i < 99; $i++) {
	for ($j = 123; $j < 9877; $j++) {
		$p = $i*$j;
		if (strlen($i . $j . $p) == 9) {
			$a = str_split($i . $j . $p);
			sort($a);
			$a = implode('', $a);
			if ($a === '123456789') {
				// echo 'I = ' . $i . '<br>';
				// echo 'J = ' . $j . '<br>';
				// echo 'P = ' . $p . '<br>';
				$products[$p] = true;
			}
		}
	}
}
foreach ($products as $p => $v) {
	$sum += $p;
}
echo 'Ответ: ' . $sum . '<br>'; // 45228
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>