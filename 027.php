<?php 
//Задача 27
// Euler discovered the remarkable quadratic formula:

// n2+n+41n2+n+41
// It turns out that the formula will produce 40 primes for the consecutive integer values 0≤n≤390≤n≤39. However, when n=40,402+40+41=40(40+1)+41n=40,402+40+41=40(40+1)+41 is divisible by 41, and certainly when n=41,412+41+41n=41,412+41+41 is clearly divisible by 41.

// The incredible formula n2−79n+1601n2−79n+1601 was discovered, which produces 80 primes for the consecutive values 0≤n≤790≤n≤79. The product of the coefficients, −79 and 1601, is −126479.

// Considering quadratics of the form:

// n2+an+bn2+an+b, where |a|<1000|a|<1000 and |b|≤1000|b|≤1000

// where |n||n| is the modulus/absolute value of nn
// e.g. |11|=11|11|=11 and |−4|=4|−4|=4
// Find the product of the coefficients, aa and bb, for the quadratic expression that produces the maximum number of primes for consecutive values of nn, starting with n=0n=0.
echo 'Задача 27<br>';
$start = microtime(true);
// Form solution 7
function isPrime($n) {
	if ($n == 1) {
		return false;
	} 
	elseif ($n < 4){
		return true;
	}
	elseif ($n % 2 == 0) {
		return false;
	}
	elseif ($n < 9) {
		return true;
	}
	elseif ($n % 3 == 0) {
		return false;
	}
	else {
		$r = floor(sqrt($n));
		$f = 5;
		 while ($f <= $r) {
			 if ($n % $f == 0) {
				 return false;
			 }
			 if ($n % ($f + 2) == 0) {
				 return false;
			 }
			 $f += 6;
		 }
		return true;
	}
}
/* Cache */
$primes = [];
for ($i = 0; $i < 1000; $i++) {
	if (isPrime($i)) {
		$primes[$i] = true;
	}
}
$maxN = 0;
$maxA = 0;
$maxB = 0;
for ($a = -1000; $a <= 1000; $a++) {
	for ($b = -1000; $b <= 1000; $b++) {
		$n = 0;
		while (isset($primes[abs($n * $n + $a * $n + $b)])) {
			$n++;
		}
		if ($n > $maxN) {
			$maxN = $n;
			$maxA = $a;
			$maxB = $b;
		}
	}
}
echo 'N = ' . $maxN . ' <br>';
echo 'A = ' . $maxA . ' <br>';
echo 'B = ' . $maxB . ' <br>';
echo 'Ответ = ' . $maxA * $maxB . ' <br>'; // -59231
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>