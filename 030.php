<?php 
//Задача 30
// Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:

// 1634 = 14 + 64 + 34 + 44
// 8208 = 84 + 24 + 04 + 84
// 9474 = 94 + 44 + 74 + 44
// As 1 = 14 is not a sum it is not included.

// The sum of these numbers is 1634 + 8208 + 9474 = 19316.

// Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
echo 'Задача 30<br>';
$start = microtime(true);
$sum = 0;
$maxPow = 5;
for ($i = 2; $i < 354295; $i++) { // 9^5 * 6 = 354294
	$sumPow = 0;
	
	// faster than $n % 10; $n / 10; ~ 4s vs 8s
	$arrayI = str_split($i . '');
	foreach ($arrayI as $v) {
		//// $sumPow += pow($v, $maxPow);
		$sumPow += $v * $v * $v * $v * $v;
	}

	/*
	//from solution 4 - slower than foreach (WTF?) ~8s vs ~4s
	$n = $i;
	while ($n > 0) {
			$v = $n % 10;
			$n = floor($n/10);
			$sumPow += $v * $v * $v * $v * $v;
	}
	*/
	if ($i == $sumPow) {
		echo 'N = ' . $i . '<br>';
		$sum += $i;
	}
}
echo 'Ответ = ' . $sum . ' <br>'; // 443839
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>