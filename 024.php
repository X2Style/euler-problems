<?php 
//Задача 24
// A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

// 012   021   102   120   201   210

// What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
echo 'Задача 24<br>';
$start = microtime(true);
$numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
$limit = 1000000;
$count = 1;

while ($count < $limit) {
    $n = count($numbers);
    $i = $j = $n - 1;
    while ($numbers[$i - 1] >= $numbers[$i]) {
        $i--;
    }
    while ($numbers[$j] <= $numbers[$i - 1]) {
        $j--;
    }
	$temp = $numbers[$i - 1];
	$numbers[$i - 1] = $numbers[$j];
	$numbers[$j] = $temp;
	$j = $n - 1;
	while ($i < $j) {
		$temp = $numbers[$i];
		$numbers[$i] = $numbers[$j];
		$numbers[$j] = $temp;
		$i++;
		$j--;
	}
	// echo ($i . ' - ' . $j . '<br>');
	$count++;
}
print_r($numbers); // 2783915460
echo '<br>' . implode($numbers) . '<br>';
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>