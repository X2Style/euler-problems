<?php 
//Задача 16
// 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
// What is the sum of the digits of the number 2^1000?
echo 'Задача 16<br>';
$number = bcpow(2, 1000);
$len = strlen($number);
$sum = 0;
for ($i = 0; $i < $len; $i++) {
	$sum += $number[$i];
}
echo $sum . '<br>'; //1366
?>