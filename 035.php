<?php 
//Задача 35
// The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

// There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

// How many circular primes are there below one million?
echo 'Задача 35<br>';

//Using function from Euler problem 007
$number = 1000000;
// start sum = count(2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, 97) = 13
$sum = 13;
$primes = [];

function isPrime($n) {
	if ($n == 1) {
		return false;
	} 
	elseif ($n < 4){
		return true;
	}
	elseif ($n % 2 == 0) {
		return false;
	}
	elseif ($n < 9) {
		return true;
	}
	elseif ($n % 3 == 0) {
		return false;
	}
	else {
		$r = floor(sqrt($n));
		$f = 5;
		 while ($f <= $r) {
			 if ($n % $f == 0) {
				 return false;
			 }
			 if ($n % ($f + 2) == 0) {
				 return false;
			 }
			 $f += 6;
		 }
		return true;
	}
}
$start = microtime(true);
for ($i = 101; $i < $number; $i += 2) {
	if (isPrime($i) 
		&& strpos($i . '', '2') === false
		&& strpos($i . '', '4') === false
		&& strpos($i . '', '5') === false
		&& strpos($i . '', '6') === false
		&& strpos($i . '', '8') === false
		&& strpos($i . '', '0') === false
		) {
		$primes[$i] = true;
	}
}
foreach ($primes as $k => $v) {
	$rotations = strlen($k);
	$a = str_split($k);
	$r = 0;
	for ($i = 0; $i < $rotations; $i++) {
		$first = array_shift($a);
		array_push($a, $first);
		$key = implode('', $a);
		if (isset($primes[$key])) {
			$r++;
			unset($primes[$key]);
		}
	}
	if ($r == $rotations) {
		$sum += $r;
	}
}
echo 'Ответ: ' . $sum . '<br>'; // 55
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>