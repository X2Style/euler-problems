<?php 
//Задача 10
// The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
// Find the sum of all the primes below two million.
echo 'Задача 10<br>';

//Using function from Euler problem 007
$number = 2000000;
$i = 3;
$sum = 2;

function isPrime($n) {
	if ($n == 1) {
		return false;
	} 
	elseif ($n < 4){
		return true;
	}
	elseif ($n % 2 == 0) {
		return false;
	}
	elseif ($n < 9) {
		return true;
	}
	elseif ($n % 3 == 0) {
		return false;
	}
	else {
		$r = floor(sqrt($n));
		$f = 5;
		 while ($f <= $r) {
			 if ($n % $f == 0) {
				 return false;
			 }
			 if ($n % ($f + 2) == 0) {
				 return false;
			 }
			 $f += 6;
		 }
		return true;
	}
}
$start = microtime(true);
for ($i = 3; $i < $number; $i += 2) {
	if (isPrime($i)) {
		$sum += $i;
	}
} 

echo 'Простое число: ' . $sum . '<br>'; //142913828922
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>