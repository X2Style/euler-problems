<?php 
//Задача 33
// The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.

// We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

// There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.

// If the product of these four fractions is given in its lowest common terms, find the value of the denominator.
echo 'Задача 33<br>';
$start = microtime(true);
// Numerator - числитель, Denomerator - знаменатель
// n10 * 10 + n1   n      
//-------------- = -; n > d; n1 = d10; n * d1 = d * n10;
// d10 * 10 + d1   d
$pd = 1;
$pn = 1;
for ($d = 10; $d < 100; $d++) {
	for ($n = 10; $n < $d; $n++) {
		$n10 = floor($n / 10);
		$n1 = $n % 10;
		$d10 = floor($d / 10);
		$d1 = $d % 10;
		if ($n1 == $d10 && ($n * $d1 == $d * $n10)) {
			echo $n10 . $n1 . ' / ' . $d10 . $d1 . ' = ' . $n10 . ' / ' . $d1 . '<br>';
			$pn *= $n;
			$pd *= $d;
		}
	}
}
function gcd($a, $b) {
    if ($a == 0 || $b == 0) {
        return abs( max(abs($a), abs($b)) );
    }
    $r = $a % $b;
    return ($r != 0) ? gcd($b, $r) : abs($b);
}
$result = $pd / gcd($pn, $pd);
echo 'Ответ: ' . $result . '<br>'; // 100
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>