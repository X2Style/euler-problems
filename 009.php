<?php 
//Задача 9
// A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
// a2 + b2 = c2
// For example, 32 + 42 = 9 + 16 = 25 = 52.
// There exists exactly one Pythagorean triplet for which a + b + c = 1000.
// Find the product abc.
echo 'Задача 9<br>';
$start = microtime(true);
$sum = 1000;
for ($b = 2; $b < ($sum - $b - 1); $b++) {
	for ($a = 1; $a < $b; $a++) {
		$c = $sum - $a - $b;
		if ($a*$a + $b*$b == $c*$c) {
			echo 'A = ' . $a . '<br>';
			echo 'B = ' . $b . '<br>';
			echo 'C = ' . $c . '<br>';
			echo 'Ответ: ' . $a * $b * $c . '<br>'; //31875000
			break;
		}
	}
}
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>