<?php 
//Задача 3
// The prime factors of 13195 are 5, 7, 13 and 29.
// What is the largest prime factor of the number 600851475143 ?
echo 'Задача 3<br>';
//$n = 13195;
$n = 600851475143;
for ($i = 2; $i*$i < $n; $i++) {
	if ($n % $i == 0) {
		$simple = true;
		for($j = 2; $j*$j < $i; $j++) {
			if ($i % $j == 0) {
				$simple = false;
				break;
			}
		}
		if ($simple) {
			$a[] = $i;
		}
	}
}
$string = implode(', ', $a);
echo 'Делится на: ' . $string . '<br>'; //71, 839, 1471, 6857
?>