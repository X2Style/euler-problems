<?php 
//Задача 25
// The Fibonacci sequence is defined by the recurrence relation:

// Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
// Hence the first 12 terms will be:

// F1 = 1
// F2 = 1
// F3 = 2
// F4 = 3
// F5 = 5
// F6 = 8
// F7 = 13
// F8 = 21
// F9 = 34
// F10 = 55
// F11 = 89
// F12 = 144
// The 12th term, F12, is the first term to contain three digits.

// What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
echo 'Задача 25<br>';
$start = microtime(true);
$solution = false;
$count = 2;
$n1 = '1';
$n2 = '1';
$sum = '0';
while (!$solution) {
	$sum = bcadd($n1, $n2);
	$n2 = $n1;
	$n1 = $sum;
	$count++;
	if (strlen($sum) > 999) {
		$solution = true;
	}
}
echo 'Ответ: ' . $count . '<br>'; // 4782
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>