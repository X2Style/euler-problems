<?php 
//Задача 31
// In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:

// 1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
// It is possible to make £2 in the following way:

// 1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
// How many different ways can £2 be made using any number of coins?
echo 'Задача 31<br>';
$start = microtime(true);
$maxPrice = 200;
$coins = [1, 2, 5, 10, 20, 50, 100, 200];
$ways[0] = 1;
for ($i = 1; $i <= $maxPrice; $i++) {
	$ways[$i] = 0;
}
 
for ($i = 0; $i < count($coins); $i++) {
    for ($j = $coins[$i]; $j <= $maxPrice; $j++) {
        $ways[$j] += $ways[$j - $coins[$i]];
		// echo $j . ' - ' . $ways[$j] . ' - ' . $ways[$j - $coins[$i]] . '<br>';
    }
}
var_dump($ways[200]); // 73682
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>