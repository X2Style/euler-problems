<?php 
//Задача 23
// A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

// A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.

// As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.

// Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
echo 'Задача 23<br>';

// Improved function from problem 21
function d($n) {
	$sqrt = floor(sqrt($n));
    $sum = 1;
    if ($n == $sqrt * $sqrt) {
        $sum += $sqrt;
        $sqrt--;
    }
    for ($i = 2; $i <= $sqrt; $i++) {
        if ($n % $i == 0) {
            $sum = $sum + $i + ($n / $i);
        }
    }
    return $sum;
}
// < 6 Mb of Memory and ~60 seconds execution time
// < 8 Mb of Memory and ~9 seconds execution time with improved D($n) function
$start = microtime(true);
set_time_limit(90);
$limit = 28123;
$numbers = [];
$sums = [];
// $totalIntegers = [];
$sum = 0;
for ($i = 12; $i <= $limit; $i++) {
	$a = d($i);
	if ($a > $i) {
		$numbers[] = $i;
		// echo $i . '<br>';
	}
}
// for ($i = 0; $i<$limit; $i++) {
	// $totalIntegers[] = $i;
// }
$c = count($numbers);
for ($i = 0; $i < $c; $i++) {
	if ($numbers[$i] * 2 > $limit) {
		break;
	}
	for ($j = $i; $j < $c; $j++) {
		$sums[$numbers[$i] + $numbers[$j]] = true; // Reduce memory usage from 1 GB ($sums[] = $numbers[$i] + $numbers[$j]), to < 10 Mb
	}
}
// $sums = array_keys($sums);
// $result = array_diff($totalIntegers, $sums);
// foreach ($result as $v) {
	// $sum += $v;
// }
for ($i = 0; $i < $limit; $i++) {
	if (!isset($sums[$i])) {
		$sum += $i;
	}
}
var_dump($sum); //4179871
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>'; // 8.65 s
?>