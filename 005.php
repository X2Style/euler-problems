<?php 
//Задача 5
// 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
// What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
echo 'Задача 5<br>';
$start = microtime(true);
$solution = false;
$dStart = 20;
while (!$solution) {
	for ($i = 20; $i > 2; $i--) {
		$solution = true;
		if ($dStart % $i != 0) {
			$solution = false;
			break;
		}
	$dStart += 20;
	}
}
echo 'Число: ' . $dStart . '<br>'; //232792560
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>'; // 12.03 s

$start = microtime(true);
$dStart = 20;
while (
	!($dStart % 20 == 0 &&
	$dStart % 19 == 0 &&
	$dStart % 18 == 0 &&
	$dStart % 17 == 0 &&
	$dStart % 16 == 0 &&
	$dStart % 15 == 0 &&
	$dStart % 14 == 0 &&
	$dStart % 13 == 0 &&
	$dStart % 12 == 0 &&
	$dStart % 11 == 0 &&
	$dStart % 10 == 0 &&
	$dStart % 9 == 0 &&
	$dStart % 8 == 0 &&
	$dStart % 7 == 0 &&
	$dStart % 6 == 0 &&
	$dStart % 5 == 0 &&
	$dStart % 4 == 0 &&
	$dStart % 3 == 0 &&
	$dStart % 2 == 0)
	) {
		$dStart += 20;
	}
echo 'Число: ' . $dStart . '<br>'; //232792560
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>'; // 2.37 s
?>