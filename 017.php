<?php 
//Задача 17
// If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

// If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?


// NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.

echo 'Задача 17<br>';
// Function from Google
$ones = array(
	1 => 'One', // 3
	2 => 'Two', // 3
	3 => 'Three', // 5
	4 => 'Four', // 4
	5 => 'Five', // 4
	6 => 'Six', // 3
	7 => 'Seven', // 5
	8 => 'Eight', // 5
	9 => 'Nine' // 4
);
$teens = array(
	1 => 'Eleven', //6
	2 => 'Twelve', //6
	3 => 'Thirteen', //8
	4 => 'Fourteen', //8
	5 => 'Fifteen', //7
	6 => 'Sixteen', //7
	7 => 'Seventeen', //9
	8 => 'Eighteen', //8
	9 => 'Nineteen' //8
);
$tens = array(
	1 => 'Ten', //3
	2 => 'Twenty', //6
	3 => 'Thirty', //6
	4 => 'Fouty', //5
	5 => 'Fifty', //5
	6 => 'Sixty', //5
	7 => 'Seventy', //7
	8 => 'Eighty', //6
	9 => 'Ninety' //6
);
function number( $i ) {
	global $ones,$teens,$tens;
	$n = str_split( $i );
	switch(count($n)) {
		case 1:
			$str = $ones[$n[0]];
		break;
		case 2:
			if ( $n[1] == 0 ) {
				$str = $tens[$n[0]];
			}
			elseif ( $i > 10 && $i < 20 ) {
				$str = $teens[$n[1]];
			}
			else {
				$str = $tens[$n[0]] . ' ' . $ones[$n[1]];
			}
		break;
		case 3:
			$str = "{$ones[$n[0]]} Hundred";
			if (  intval($n[1].$n[2]) !== 0 ) {
				$str .= ' and ' . number( intval($n[1].$n[2]) );
			}
		break;
		case 4:
			$str = "{$ones[$n[0]]} Thousand";
		break;
	}
	return $str;
}
$total = 0;
$i = 1;
while( $i <= 1000 ) {
	$str = number( $i );
	$total += strlen( str_replace(' ','',$str) );
	// echo $str . '<br />';
	$i++;
}
echo $total . '<br>'; // 21124

/*
1-9
3+3+5+4+4+3+5+5+4 = 36

10-19
3+6+6+8+8+7+7+9+8+8 = 70

20-99
6+6+5+5+5+7+6+6 = 46 /20, 30 ...

36*8 + 46*10 = 748

100-999
9 * 7 = 63 / hundred
854 * 9 = 7686 / 101 - 199 ... 999
99*9 * 10 = 8910 /hundred and
36*100 = 3600 / one hund... two ...

1000
3 + 8 = 11

36+70+748+63+7686+8910+3600+11 = 21124

*/
?>