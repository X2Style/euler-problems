<?php 
//Задача 6
// The sum of the squares of the first ten natural numbers is,
// 12 + 22 + ... + 102 = 385
// The square of the sum of the first ten natural numbers is,
// (1 + 2 + ... + 10)2 = 552 = 3025
// Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
// Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
echo 'Задача 6<br>';
//brute force
$n = 100;
$sum = 0;
$sums = 0;
$square = 0;
for ($i = 0; $i <= $n; $i++) {
	$sum += $i;
	$sums += $i*$i;
}
$result = $sum*$sum - $sums;
echo 'Сумма квадратов чисел: ' . $sums . '<br>Сумма чисел в квадрате: ' . $sum*$sum . '<br>Разность: ' . $result . '<br>';

//По формулам
//Сумма натуральных чисел = n(n+1)/2
//Сумма квадратов натуральных чисел n(n+1)(2n+1)/6

$n = 100;
$sum = $n * ($n + 1)/2;
$sums = $n * ($n + 1) * (2 * $n + 1) / 6;
$result = $sum*$sum - $sums;
echo 'Сумма квадратов чисел: ' . $sums . '<br>Сумма чисел в квадрате: ' . $sum*$sum . '<br>Разность: ' . $result . '<br>'; // 25164150
?>