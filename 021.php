<?php 
//Задача 21
// Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
// If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

// For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

// Evaluate the sum of all the amicable numbers under 10000.
echo 'Задача 21<br>';
// my function
// function d($n) {
	// $sum = 1;
	// for ($i = 2; $i <= $n/2; $i++) {
		// if ($n % $i == 0) {
			// $sum += $i;
		// }
	// }
	// return $sum;
// }
//Improved function
function d($n) {
	$sqrt = floor(sqrt($n));
    $sum = 1;
    if ($n == $sqrt * $sqrt) {
        $sum += $sqrt;
        $sqrt--;
    }
    for ($i = 2; $i <= $sqrt; $i++) {
        if ($n % $i == 0) {
            $sum = $sum + $i + ($n / $i);
        }
    }
    return $sum;
}
$numbers = [0];
$limit = 10000;
$result = 0;
$start = microtime(true);
for ($i = 1; $i < 10000; $i++) {
	$numbers[$i] = d($i);
}
for ($i = 1; $i < $limit; $i++) {
	if ($numbers[$i] < $limit && $numbers[$i] != $numbers[$numbers[$i]] && $i == $numbers[$numbers[$i]]) {
		$result += $numbers[$i];
	}
}

echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
echo $result . '<br>'; //31626
?>