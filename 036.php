<?php 
//Задача 36
// The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.

// Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.

// (Please note that the palindromic number, in either base, may not include leading zeros.)
echo 'Задача 36<br>';
$start = microtime(true);
$limit = 1000000;
$pal = [];
$sum = 0;
function isPalindromic($n) {
	if ($n % 10 !== 0) {
		// $reversed = 0;
		// while ($n > 0) {
			// $reversed = $reversed*10 + ($n % 10);
			// $n = floor($n / 10);
		// }
		if ($n == strrev($n . '')) {
			return true;
		}
	}
	return false;
}
for ($i = $limit; $i > 0; $i--) {
	if (isPalindromic($i)) {
		$pal[$i] = true;
	}
}
foreach ($pal as $k => $v) {
	if (isPalindromic(decbin($k))) {
		$sum += $k;
		echo $k . '<br>';
	}
}
echo 'Ответ: ' . $sum . '<br>'; // 872187
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>'; // 7.1s
$start = microtime(true);
$sum = 0;
for ($i = 1; $i < 1000000; $i++) {
	$binI = decbin($i);
	if ($i == strrev($i) && $binI == strrev($binI)) {
		$sum += $i;
	}
}
echo 'Ответ: ' . $sum . '<br>'; // 872187
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>'; // 7.0s
?>