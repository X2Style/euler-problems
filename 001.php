<?php 
// Задача 1
// If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
// Find the sum of all the multiples of 3 or 5 below 1000.
echo 'Задача 1<br>';
$sum = $sum2 = 0;
for ($i = 0; $i < 10; $i++) {
	if ($i % 3 == 0 || $i % 5 == 0) {
		$sum += $i;
	}
}
$start = microtime(true);
for ($i = 0; $i < 1000; $i++) {
	if ($i % 3 == 0 || $i % 5 == 0) {
		$sum2 += $i;
	}
}
echo 'Сумма чисел меньше 10: ' . $sum . '<br>Сумма чисел меньше 1000: ' . $sum2 . '<br>';
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>