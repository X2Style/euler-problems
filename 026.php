<?php 
//Задача 26
// A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

// 1/2	= 	0.5
// 1/3	= 	0.(3)
// 1/4	= 	0.25
// 1/5	= 	0.2
// 1/6	= 	0.1(6)
// 1/7	= 	0.(142857)
// 1/8	= 	0.125
// 1/9	= 	0.(1)
// 1/10	= 	0.1
// Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

// Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
echo 'Задача 26<br>';
// My WHTTHFCK solution
$start = microtime(true);
$d = 1000;
$maxD = 0;
$maxCount = 0;
$maxRes = 0;
for ($i = 7; $i <= $d; $i++) {
	$result = substr(bcdiv(1, $i, 2000), 2);
	for ($j = 7; $j < 1001; $j++) {
		if (substr($result, 0, $j) === substr($result, $j, $j)) {
			if ($j > $maxCount) {
				$maxCount = $j;
				$maxD = $i;
				$maxRes = $result;
			}
			/* echo 'MC = ' . $j . 'Digit = ' . $i . '<br>'; */
			break;
		}
	}
}
echo 'Ответ: ' . $maxD . '<br>'; // 983
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';

// Guys from google says "Use MATAN" :)))
// This is a useful application of Fermat’s little theorem that says: 1/d has a cycle of n digits if 10n − 1 mod d = 0 for prime d. It also follows that a prime number in the denominator, d, can yield up to d − 1 repeating decimal digits. We need to find the largest prime under 1000 that has d − 1 digits. This is called a full reptend prime.
$start = microtime(true);
$maxCount = 0;
$maxD = 0;
for ($i = 7; $i <= $d; $i++){
    $count = 0;
    $value = 10 % $i;
    $limit = $d;
    while ($value != 1 && $limit > 0){
      $value *= 10;
      $value %= $i;
      $count++;
      $limit--;
    }
    if ($count > $maxCount && $limit > 1){
      $maxCount = $count;
      $maxD = $i;
    }
  }
echo 'Ответ: ' . $maxD . '<br>'; // 983
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>