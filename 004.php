<?php 
//Задача 4
// A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
// Find the largest palindrome made from the product of two 3-digit numbers.
echo 'Задача 4<br>';

// My solution
$start = microtime(true);
$pol = 0;
$maxi = 0;
$maxj = 0;
for ($i = 999; $i > 99; $i--) {
	for ($j = $i; $j > 99; $j--) {
		$n = $i*$j;
		if ($n < $pol) {
			break;
		}
		$original = $n;
		$reversed = 0;
		while ($n > 0) {
			$reversed = $reversed*10 + ($n % 10);
			$n = floor($n/10);
		}
		if ($original == $reversed && $pol < $original) {
			$pol = $original;
			$maxi = $i;
			$maxj = $j;
			break;
		}
	}
}
echo 'Число палиндром:' . $pol . ', при ' . $maxi . ' * ' . $maxj . '<br>'; //906609
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';

// Solution from Google
$start = microtime(true);
$solution = false;
$maxHalf = 998; //(999*999 = 998001)
while (!$solution) {
	$n = $maxHalf;
	$reversed = 0;
	while ($n > 0) {
		$reversed = $reversed*10 + ($n % 10);
		$n = floor($n/10);
	}
	if ($reversed < 100) {
		$pol = $maxHalf . '0' . $reversed;
	} else {
	$pol = $maxHalf . $reversed;}
	for ($i = 999; $i > 99; $i--) {
		if (($pol / $i) > 999 || $i*$i < $pol) {
            break;
        }
		if ($pol % $i == 0) {
			$solution = true;
		}
	}
	$maxHalf--;
}
echo 'Число палиндром:' . $pol . '<br>'; //906609
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
?>